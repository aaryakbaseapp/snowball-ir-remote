<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="15" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="14" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="10" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BaseApp">
<packages>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
</package>
<package name="C1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TQFP-32">
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.2032" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.2032" layer="21"/>
<circle x="-3.6576" y="3.683" radius="0.1524" width="0.2032" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.3" layer="1"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.3" layer="1"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="10" x="-2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="15" x="2" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.3" layer="1"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.3" layer="1"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.3" layer="1"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.3" layer="1"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.3" layer="1"/>
<smd name="25" x="2.8" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="26" x="2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="27" x="1.2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="28" x="0.4" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="31" x="-2" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.3" dy="1.27" layer="1"/>
<text x="-3.175" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
</package>
<package name="BATTERY_HOLDER">
<smd name="+" x="-14.65" y="0" dx="5" dy="3.5" layer="1" rot="R90"/>
<smd name="ADH" x="0" y="0" dx="11" dy="7" layer="1" rot="R180"/>
<smd name="-" x="14.65" y="0" dx="5" dy="3.5" layer="1" rot="R90"/>
<text x="-3.22" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.22" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-13.38" y1="2.54" x2="-13.38" y2="3.81" width="0.127" layer="21"/>
<wire x1="-13.38" y1="3.81" x2="-8.3" y2="8" width="0.127" layer="21" curve="-90"/>
<wire x1="-13.38" y1="-2.54" x2="-13.38" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-13.38" y1="-3.81" x2="-8.3" y2="-8" width="0.127" layer="21" curve="90"/>
<wire x1="13.29" y1="2.54" x2="13.29" y2="3.81" width="0.127" layer="21"/>
<wire x1="13.29" y1="3.81" x2="9.48" y2="8" width="0.127" layer="21" curve="90"/>
<wire x1="13.47" y1="-2.54" x2="13.47" y2="-3.81" width="0.127" layer="21"/>
<wire x1="13.47" y1="-3.81" x2="8.39" y2="-8" width="0.127" layer="21" curve="-90"/>
<wire x1="-8.21" y1="8" x2="9.57" y2="8" width="0.127" layer="21"/>
<wire x1="-8.21" y1="-8" x2="8.3" y2="-8" width="0.127" layer="21"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="-12" y="-1" size="1.27" layer="21">+</text>
<text x="11" y="-1" size="1.27" layer="21">-</text>
</package>
<package name="2*AA_BATTERY_HOLDER">
<wire x1="-29.755" y1="16.45" x2="29.755" y2="16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="-16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="16.45" x2="-29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="29.755" y1="16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<hole x="0" y="-7.5" drill="3.43"/>
<hole x="0" y="7.49" drill="3.43"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
</package>
<package name="AA-BATTERY-HOLDER-DIMENSION">
<wire x1="-29.6" y1="16.5" x2="29.6" y2="16.5" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
<wire x1="-30" y1="16.1" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
<wire x1="30" y1="16.1" x2="30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="16.5" x2="-30" y2="16.1" width="0.127" layer="20"/>
<wire x1="29.6" y1="16.5" x2="30" y2="16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="30" y1="-16.1" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
</package>
<package name="LI-BATTERY-HOLDER">
<wire x1="10.45" y1="-38.85" x2="-10.45" y2="-38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-38.85" x2="-10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="38.85" x2="10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="10.45" y1="38.85" x2="10.45" y2="-38.85" width="0.4064" layer="21"/>
<hole x="0" y="27.805" drill="3.2"/>
<hole x="0" y="-27.805" drill="3.2"/>
<text x="-1.45" y="20.17" size="5.08" layer="21" font="vector" ratio="15">+</text>
<text x="-1.45" y="-25.83" size="5.08" layer="21" font="vector" ratio="15">-</text>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="+" x="0" y="36.45" drill="2.794" diameter="3.81" shape="long"/>
<pad name="-" x="0" y="-36.45" drill="2.794" diameter="3.81" shape="long"/>
</package>
<package name="COINCELL-SMT">
<smd name="P$1" x="0" y="10.7" dx="3" dy="3.5" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-10.7" dx="3.5" dy="4" layer="1"/>
<wire x1="-7.18" y1="8.4" x2="7.18" y2="8.4" width="0.127" layer="51" curve="-126.869898"/>
<wire x1="7.18" y1="-3.8" x2="-7.18" y2="-3.8" width="0.127" layer="51" curve="-126.869898"/>
<wire x1="-7.2" y1="8.4" x2="-7.2" y2="-8.4" width="0.127" layer="21"/>
<wire x1="7.2" y1="8.4" x2="7.2" y2="-8.4" width="0.127" layer="21"/>
<wire x1="-7.2" y1="-8.4" x2="-2" y2="-11.9" width="0.127" layer="21"/>
<wire x1="7.2" y1="-8.4" x2="2" y2="-11.9" width="0.127" layer="21"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
</package>
<package name="COINCELL-DIP">
<pad name="+" x="-13.45" y="1.25" drill="1.1"/>
<pad name="+@1" x="-13.45" y="-1.25" drill="1.1"/>
<pad name="-" x="13.45" y="0" drill="1.1"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="21"/>
<wire x1="15" y1="3.5" x2="15" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-15" y1="3.5" x2="-11" y2="3.5" width="0.127" layer="21"/>
<wire x1="-15" y1="-3.5" x2="-11" y2="-3.5" width="0.127" layer="21"/>
<wire x1="11" y1="3.5" x2="15" y2="3.5" width="0.127" layer="21"/>
<wire x1="15" y1="-3.5" x2="11" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="10" x2="5.5" y2="10" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-10" x2="5.5" y2="-10" width="0.127" layer="21"/>
<wire x1="-11" y1="3.5" x2="-11" y2="4.5" width="0.127" layer="21"/>
<wire x1="-11" y1="4.5" x2="-5.5" y2="10" width="0.127" layer="21" curve="-90"/>
<wire x1="11" y1="3.5" x2="11" y2="4.5" width="0.127" layer="21"/>
<wire x1="11" y1="4.5" x2="5.5" y2="10" width="0.127" layer="21" curve="90"/>
<wire x1="-5.5" y1="-10" x2="-11" y2="-3.5" width="0.127" layer="21" curve="-99.527296"/>
<wire x1="5.5" y1="-10" x2="11" y2="-3.5" width="0.127" layer="21" curve="99.527296"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="-12" y="-1" size="1.778" layer="21" ratio="15">+</text>
<text x="11" y="-0.5" size="1.778" layer="21" ratio="15">-</text>
<text x="-4" y="4" size="1.27" layer="21" ratio="15">&gt;NAME</text>
<text x="-4" y="-4.5" size="1.27" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="BC-2400-ND">
<pad name="+@1" x="-10.475" y="0" drill="2.03"/>
<pad name="+@2" x="10.475" y="0" drill="2.03"/>
<wire x1="-12.475" y1="3.5" x2="12.525" y2="3.5" width="0.127" layer="21"/>
<wire x1="12.525" y1="3.5" x2="12.525" y2="-3.5" width="0.127" layer="21"/>
<wire x1="12.525" y1="-3.5" x2="-12.475" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-12.475" y1="-3.5" x2="-12.475" y2="3.5" width="0.127" layer="21"/>
<smd name="GND" x="0" y="0" dx="15" dy="15" layer="1" roundness="100"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CR2032-THRU">
<wire x1="10.34" y1="3.8" x2="13.32" y2="3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="3.8" x2="13.32" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="-3.8" x2="10.34" y2="-3.8" width="0.2032" layer="21"/>
<circle x="0.06" y="0.1" radius="10" width="0.2032" layer="51"/>
<pad name="2" x="-8.15" y="0" drill="1.3" rot="R90"/>
<pad name="1" x="11.85" y="0" drill="1.3" rot="R90"/>
<text x="-15.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-15.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="8.6" y="-0.7" size="1.27" layer="51">+</text>
<text x="-6.4" y="-0.7" size="1.27" layer="51">-</text>
<wire x1="-10.54" y1="3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="10.34" y1="3.8" x2="-10.54" y2="3.8" width="0.2032" layer="21" curve="139.856795"/>
<wire x1="10.34" y1="-3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21" curve="-139.856795"/>
</package>
<package name="3-AA-BATTERY-HOLDER">
<wire x1="-30" y1="25" x2="30" y2="25" width="0.127" layer="21"/>
<wire x1="-30" y1="-25" x2="30" y2="-25" width="0.127" layer="21"/>
<wire x1="-30" y1="-25" x2="-30" y2="25" width="0.127" layer="21"/>
<wire x1="30" y1="-25" x2="30" y2="25" width="0.127" layer="21"/>
<hole x="0" y="15" drill="2.5"/>
<hole x="0" y="-15" drill="2.5"/>
<pad name="-" x="26.07" y="-0.7" drill="1.2" shape="square"/>
<pad name="+" x="26.07" y="-16.5" drill="1.2"/>
<text x="21" y="-18.27" size="3.81" layer="51">+</text>
<text x="21" y="-2.27" size="3.81" layer="51">-</text>
<text x="-2" y="21" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-21" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="CR2032-VERT">
<pad name="-" x="0" y="0" drill="0.8" shape="square"/>
<pad name="+@1" x="-5" y="-3.81" drill="0.8"/>
<pad name="+@2" x="5" y="-3.81" drill="0.8"/>
<wire x1="-11.5" y1="1.27" x2="-11.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-11.5" y1="-5.08" x2="11.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="11.5" y1="-5.08" x2="11.5" y2="1.27" width="0.127" layer="21"/>
<wire x1="11.5" y1="1.27" x2="-11.5" y2="1.27" width="0.127" layer="21"/>
<text x="1.27" y="0" size="1.27" layer="51">-ve</text>
<text x="-8.89" y="-3.81" size="1.27" layer="51">+ve</text>
<text x="6.35" y="-3.81" size="1.27" layer="51">+ve</text>
</package>
<package name="BC-2003">
<wire x1="-13.97" y1="3.5" x2="13.97" y2="3.5" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.5" x2="13.97" y2="-3.5" width="0.127" layer="21"/>
<wire x1="13.97" y1="-3.5" x2="-13.97" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-3.5" x2="-13.97" y2="3.5" width="0.127" layer="21"/>
<smd name="GND" x="0" y="0" dx="10" dy="5" layer="1"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
<smd name="+@0" x="-11.905" y="0" dx="6" dy="3" layer="1" rot="R90"/>
<smd name="+@1" x="11.905" y="0" dx="6" dy="3" layer="1" rot="R90"/>
</package>
<package name="BK-912">
<smd name="GND" x="0" y="0" dx="16" dy="16" layer="1" roundness="100"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
<smd name="+@0" x="-11" y="0" dx="5.5" dy="3" layer="1" rot="R90"/>
<smd name="+@1" x="11" y="0" dx="5.5" dy="3" layer="1" rot="R90"/>
<wire x1="-9.525" y1="10.16" x2="9.525" y2="10.16" width="0.127" layer="21"/>
<wire x1="-9.525" y1="10.16" x2="-10.795" y2="8.89" width="0.127" layer="21"/>
<wire x1="-10.795" y1="8.89" x2="-10.795" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-5.08" x2="-5.08" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-8.89" x2="-3.175" y2="-6.985" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-6.985" x2="3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="5.08" y2="-8.89" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="-8.89" x2="10.795" y2="-5.08" width="0.127" layer="21"/>
<wire x1="10.795" y1="-5.08" x2="10.795" y2="8.89" width="0.127" layer="21"/>
<wire x1="10.795" y1="8.89" x2="9.525" y2="10.16" width="0.127" layer="21"/>
</package>
<package name="1*AA_BATTERY_HOLDER">
<pad name="-" x="-26" y="0" drill="1.1"/>
<pad name="+" x="26" y="0" drill="1.1"/>
<hole x="-23.24" y="6" drill="2.7"/>
<hole x="23.24" y="-6" drill="2.7"/>
<wire x1="-29" y1="8.5" x2="29" y2="8.5" width="0.127" layer="21"/>
<wire x1="29" y1="8.5" x2="29" y2="-8.5" width="0.127" layer="21"/>
<wire x1="29" y1="-8.5" x2="-29" y2="-8.5" width="0.127" layer="21"/>
<wire x1="-29" y1="-8.5" x2="-29" y2="8.5" width="0.127" layer="21"/>
<text x="22" y="-2" size="2.54" layer="21">+</text>
<text x="-25" y="-3" size="2.54" layer="21">-</text>
<text x="-7" y="4" size="2.54" layer="25">&gt;NAME</text>
<text x="-7" y="-7" size="2.54" layer="27">&gt;VALUE</text>
</package>
<package name="TACTILE-PTH">
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="TACTILE-SMD-4.5X4.5">
<smd name="1" x="1.5" y="3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="-3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-1.5" y="-3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-1.5" y="3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="21"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="21"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LS_A67K">
<smd name="C" x="-1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<smd name="A" x="1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="-2.5" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="0" y="1.524" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="LS_Y876">
<smd name="A" x="1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<smd name="C" x="-1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<wire x1="-1.016" y1="-0.762" x2="1.016" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.762" x2="-1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.762" x2="-1.016" y2="-0.762" width="0.127" layer="21"/>
<circle x="0" y="-0.254" radius="0.254" width="0.127" layer="21"/>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LED0603">
<wire x1="0.45" y1="0.4" x2="-0.45" y2="0.4" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.4" x2="-0.45" y2="-0.4" width="0.1016" layer="51"/>
<smd name="C" x="0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<smd name="A" x="-0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.2" y1="-0.2" x2="1.1" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="-1.1" y1="-0.2" x2="-0.2" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="0.075" y1="0.225" x2="0.225" y2="0.525" layer="21" rot="R270"/>
<rectangle x1="0.075" y1="-0.525" x2="0.225" y2="-0.225" layer="21" rot="R270"/>
<rectangle x1="0" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<wire x1="1.5" y1="0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IR-LED">
<circle x="0" y="0" radius="1.5" width="0.127" layer="51"/>
<pad name="+" x="-1.27" y="0" drill="0.5"/>
<pad name="-" x="1.27" y="0" drill="0.5"/>
<wire x1="1.5" y1="1.57" x2="1.5" y2="-1.57" width="0.127" layer="21"/>
<wire x1="-2.2" y1="0" x2="0" y2="2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.2" y1="0" x2="0" y2="-2.2" width="0.127" layer="21" curve="90"/>
<wire x1="0" y1="2.2" x2="1.5" y2="1.6" width="0.127" layer="21" curve="-43.602819"/>
<wire x1="0" y1="-2.2" x2="1.5" y2="-1.6" width="0.127" layer="21" curve="43.602819"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TAC-SMD-12X12">
<smd name="1" x="2.5" y="6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.5" y="-6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.5" y="-6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.5" y="6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TACTILE">
<smd name="1" x="2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOT23-R">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6524" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.5724" y1="0.6604" x2="-0.5136" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6524" width="0.1524" layer="21"/>
<wire x1="0.5636" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="0.4224" y1="-0.6604" x2="-0.4364" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="1.778" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.778" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT223">
<wire x1="-3.124" y1="1.731" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="-3.124" y1="1.731" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<smd name="1" x="-2.2606" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="2" x="0.0254" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="3" x="2.3114" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="4" x="0" y="3.1496" dx="3.81" dy="2.0066" layer="1"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.794" y="-5.842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="1.778" x2="1.524" y2="3.302" layer="51"/>
<rectangle x1="-2.667" y1="-3.302" x2="-1.905" y2="-1.778" layer="51"/>
<rectangle x1="1.905" y1="-3.302" x2="2.667" y2="-1.778" layer="51"/>
<rectangle x1="-0.381" y1="-3.302" x2="0.381" y2="-1.778" layer="51"/>
</package>
<package name="SOT23-W">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="EDG-03">
<wire x1="-4.572" y1="-4.953" x2="4.572" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="4.572" y1="4.953" x2="-4.572" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="4.953" x2="-4.572" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-1.651" x2="-4.064" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.651" x2="-4.064" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.651" x2="-4.572" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-1.651" x2="-4.572" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="1.651" x2="-4.572" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-4.953" x2="4.572" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="4.572" y1="1.651" x2="4.064" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.651" x2="4.064" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.651" x2="4.572" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="4.572" y1="1.651" x2="4.572" y2="4.953" width="0.1524" layer="21"/>
<wire x1="4.572" y1="-1.651" x2="4.572" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.778" y1="1.905" x2="1.778" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.778" y1="1.905" x2="3.302" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.905" x2="3.302" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.905" x2="1.778" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.905" x2="-0.762" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.905" x2="0.762" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.905" x2="-0.762" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.905" x2="-3.302" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.905" x2="-1.778" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-1.905" x2="-1.778" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-1.905" x2="-3.302" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.572" y="-6.604" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.572" y="5.334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="2.032" y1="-0.381" x2="3.048" y2="0" layer="21"/>
<rectangle x1="2.032" y1="-1.016" x2="3.048" y2="-0.635" layer="21"/>
<rectangle x1="2.032" y1="-1.651" x2="3.048" y2="-1.27" layer="21"/>
<rectangle x1="-0.508" y1="-0.381" x2="0.508" y2="0" layer="21"/>
<rectangle x1="-0.508" y1="-1.016" x2="0.508" y2="-0.635" layer="21"/>
<rectangle x1="-0.508" y1="-1.651" x2="0.508" y2="-1.27" layer="21"/>
<rectangle x1="-3.048" y1="-0.381" x2="-2.032" y2="0" layer="21"/>
<rectangle x1="-3.048" y1="-1.016" x2="-2.032" y2="-0.635" layer="21"/>
<rectangle x1="-3.048" y1="-1.651" x2="-2.032" y2="-1.27" layer="21"/>
</package>
<package name="B2,54">
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="LRTB-GFTM">
<smd name="1" x="-1.3" y="0.8" dx="1.2" dy="0.6" layer="1"/>
<smd name="2" x="-1.3" y="0" dx="1.2" dy="0.6" layer="1"/>
<smd name="3" x="-1.3" y="-0.8" dx="1.2" dy="0.6" layer="1"/>
<smd name="6" x="1.3" y="0.8" dx="1.2" dy="0.6" layer="1"/>
<smd name="5" x="1.3" y="0" dx="1.2" dy="0.6" layer="1"/>
<smd name="4" x="1.3" y="-0.8" dx="1.2" dy="0.6" layer="1"/>
<wire x1="1.3" y1="1.5" x2="1.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.3" y1="1.5" x2="-1.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.5" x2="-1.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.3" y1="-1.5" x2="-1.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.4" x2="1.3" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.3" y1="1.5" x2="-1.3" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.3" y1="1.5" x2="-1.3" y2="1.4" width="0.127" layer="51"/>
<wire x1="1.3" y1="-1.4" x2="1.3" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.3" y1="-1.5" x2="-1.3" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-1.5" x2="-1.3" y2="-1.4" width="0.127" layer="51"/>
<text x="2.6" y="3.2" size="1.27" layer="25" rot="R270">&gt;NAME</text>
<text x="-3.7" y="3.2" size="1.27" layer="27" rot="R270">&gt;VALUE</text>
<text x="-3.7" y="3.2" size="1.27" layer="27" rot="R270">&gt;VALUE</text>
<wire x1="1.016" y1="0" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.4" x2="0.4" y2="0" width="0.127" layer="21"/>
<wire x1="0.4" y1="0" x2="0.4" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.4" x2="-0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0" x2="0.4" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0.3" x2="-0.2" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0" x2="-1.016" y2="0" width="0.127" layer="21"/>
</package>
<package name="LED-3MM">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CPOL_B">
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0.1016" layer="21" curve="-128.186984"/>
<wire x1="-1.75" y1="-0.85" x2="1.75" y2="-0.85" width="0.1016" layer="21" curve="128.186984"/>
<wire x1="-2.1" y1="0.85" x2="-2.1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="21"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="-0.85" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="1.5" x2="-1.2" y2="-1.5" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.95" width="0.1016" layer="51"/>
<smd name="-" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="+" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-2.15" y="2.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.15" y="-3.275" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-0.35" x2="-1.85" y2="0.35" layer="51"/>
<rectangle x1="1.9" y1="-0.35" x2="2.3" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-1.25" y="1.45"/>
<vertex x="-1.7" y="0.85"/>
<vertex x="-1.85" y="0.35"/>
<vertex x="-1.85" y="-0.4"/>
<vertex x="-1.7" y="-0.85"/>
<vertex x="-1.25" y="-1.4"/>
<vertex x="-1.25" y="1.4"/>
</polygon>
</package>
<package name="CPOL_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-G">
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="51"/>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.1016" layer="51"/>
<wire x1="-5" y1="0.95" x2="-5" y2="5" width="0.1016" layer="21"/>
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="0.95" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.59" x2="4.79" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<smd name="+" x="3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL_F_8X10">
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="51"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="1" x2="-4.1" y2="5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="21"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="1" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1" x2="4.1" y2="-2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="-1" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="4" width="0.001" layer="51"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3.55" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="+" x="3.55" y="0" dx="4" dy="1.6" layer="1"/>
<text x="-1.75" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-2.375" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.85" y1="-0.45" x2="-3.9" y2="0.45" layer="51"/>
<rectangle x1="3.9" y1="-0.45" x2="4.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL_E">
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.8" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.8" y="-2.225" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL-16X16">
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="0.95" x2="-7.54" y2="8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<smd name="+" x="5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="EIA3528-KIT">
<wire x1="-0.9" y1="-1.6" x2="-3.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.6" x2="-3.1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.7" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.55" x2="3.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-1.2" x2="3.1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.1" y1="1.25" x2="2.7" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.4" layer="21" style="longdash"/>
<smd name="C" x="-1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<smd name="A" x="1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="E5-10.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="5.08" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="E5-12.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="6.5" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="C2917">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="E2-5">
<wire x1="-1.524" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.778" x2="-0.762" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.524" x2="-1.016" y2="2.032" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="-" x="1.016" y="0" drill="0.8128" diameter="1.27" shape="octagon"/>
<pad name="+" x="-1.016" y="0" drill="0.8128" diameter="1.27"/>
<text x="2.54" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.54" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="51"/>
</package>
<package name="E3.5-8">
<wire x1="-3.429" y1="1.143" x2="-2.667" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="4.064" width="0.1524" layer="21"/>
<pad name="-" x="1.778" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.778" y="0" drill="0.8128" diameter="1.6002"/>
<text x="3.302" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2.5-6">
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="2.667" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="C1206-POL">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
</package>
<package name="CPOL_10.3X10.3X10">
<wire x1="-5.2" y1="5.2" x2="1.5" y2="5.2" width="0.1016" layer="51"/>
<wire x1="1.5" y1="5.2" x2="5.2" y2="3" width="0.1016" layer="51"/>
<wire x1="5.2" y1="3" x2="5.2" y2="-3" width="0.1016" layer="51"/>
<wire x1="5.2" y1="-3" x2="1.3" y2="-5.2" width="0.1016" layer="51"/>
<wire x1="1.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.1016" layer="51"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="5.2" width="0.1016" layer="51"/>
<wire x1="-5.2" y1="1" x2="-5.2" y2="5.2" width="0.1016" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="1.5" y2="5.2" width="0.1016" layer="21"/>
<wire x1="1.5" y1="5.2" x2="5.2" y2="3" width="0.1016" layer="21"/>
<wire x1="5.2" y1="3" x2="5.2" y2="1" width="0.1016" layer="21"/>
<wire x1="5.2" y1="-1" x2="5.2" y2="-3" width="0.1016" layer="21"/>
<wire x1="5.2" y1="-3" x2="1.3" y2="-5.2" width="0.1016" layer="21"/>
<wire x1="1.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.1016" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="4" width="0.001" layer="51"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3.75" y="0" dx="4.5" dy="1.6" layer="1"/>
<smd name="+" x="3.75" y="0" dx="4.5" dy="1.6" layer="1"/>
<text x="-1.75" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-2.375" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.85" y1="-0.45" x2="-3.9" y2="0.45" layer="51"/>
<rectangle x1="3.9" y1="-0.45" x2="4.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="E2.5-6.3-7">
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.2" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="2.667" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2.5-8/6">
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="4.1" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="2.667" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="TACTILE-SMD-6X6">
<smd name="1" x="2.3" y="4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.3" y="-4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-2.3" y="-4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-2.3" y="4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="PINHEAD-2X3-SMT">
<smd name="1" x="0" y="-2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="0" y="2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="2.54" y="2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="5.08" y="2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="-2.55" dx="4" dy="1.2" layer="1" rot="R90"/>
<wire x1="-1" y1="4.5" x2="6" y2="4.5" width="0.127" layer="21"/>
<wire x1="6" y1="4.5" x2="6" y2="-5" width="0.127" layer="21"/>
<wire x1="6" y1="-5" x2="-1" y2="-5" width="0.127" layer="21"/>
<wire x1="-1" y1="-5" x2="-1" y2="4.5" width="0.127" layer="21"/>
<text x="0" y="5" size="1.27" layer="25">&gt;name</text>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="ATMEGA328">
<wire x1="-20.32" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<text x="-20.32" y="33.782" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-25.4" y="0" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-25.4" y="5.08" length="middle"/>
<pin name="GND@3" x="-25.4" y="-22.86" length="middle"/>
<pin name="GND@5" x="-25.4" y="-25.4" length="middle"/>
<pin name="VCC@4" x="-25.4" y="22.86" length="middle"/>
<pin name="VCC@6" x="-25.4" y="20.32" length="middle"/>
<pin name="AGND" x="-25.4" y="-20.32" length="middle"/>
<pin name="AREF" x="-25.4" y="15.24" length="middle"/>
<pin name="AVCC" x="-25.4" y="25.4" length="middle"/>
<pin name="PB4(MISO)" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ADC7" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="ADC6" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-25.4" y="30.48" length="middle" function="dot"/>
</symbol>
<symbol name="BATTERY">
<pin name="-" x="-5.08" y="0" visible="off" length="short"/>
<pin name="+" x="2.54" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<text x="5.08" y="5.08" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VDD">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="0" y2="3.81" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90">GND</text>
</symbol>
<symbol name="TACTILE">
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="0" y2="0.635" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="0" visible="pin" length="short"/>
<pin name="2" x="-7.62" y="-2.54" visible="pin" length="short"/>
<pin name="3" x="7.62" y="0" visible="pin" length="short" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="0.635" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<text x="7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="SW_DIP-3">
<wire x1="-3.302" y1="3.048" x2="0" y2="3.048" width="0.1524" layer="94"/>
<wire x1="3.302" y1="3.048" x2="3.302" y2="2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="2.032" x2="-3.302" y2="3.048" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="3.302" y2="3.048" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="-3.302" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.302" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.302" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-0.508" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-0.508" x2="-3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.508" x2="-3.302" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.032" x2="3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-3.048" x2="-3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.048" x2="-3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.794" x2="1.905" y2="2.794" width="0" layer="94"/>
<wire x1="1.397" y1="2.286" x2="1.905" y2="2.286" width="0" layer="94"/>
<wire x1="1.397" y1="0.254" x2="1.905" y2="0.254" width="0" layer="94"/>
<wire x1="1.397" y1="-0.254" x2="1.905" y2="-0.254" width="0" layer="94"/>
<wire x1="1.397" y1="-2.286" x2="1.905" y2="-2.286" width="0" layer="94"/>
<wire x1="1.397" y1="-2.794" x2="1.905" y2="-2.794" width="0" layer="94"/>
<text x="-5.08" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="4.826" y="-2.667" size="1.27" layer="94" ratio="10" rot="R90">1</text>
<text x="4.826" y="-0.381" size="1.27" layer="94" ratio="10" rot="R90">2</text>
<text x="4.826" y="2.159" size="1.27" layer="94" ratio="10" rot="R90">3</text>
<text x="-3.556" y="-4.699" size="1.27" layer="94" ratio="10" rot="R90">ON</text>
<rectangle x1="0.381" y1="2.286" x2="1.397" y2="2.794" layer="94"/>
<rectangle x1="1.905" y1="2.286" x2="2.921" y2="2.794" layer="94"/>
<rectangle x1="0.381" y1="-0.254" x2="1.397" y2="0.254" layer="94"/>
<rectangle x1="1.905" y1="-0.254" x2="2.921" y2="0.254" layer="94"/>
<rectangle x1="0.381" y1="-2.794" x2="1.397" y2="-2.286" layer="94"/>
<rectangle x1="1.905" y1="-2.794" x2="2.921" y2="-2.286" layer="94"/>
<pin name="4" x="-7.62" y="2.54" visible="pad" length="short" direction="pas" swaplevel="3"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="6" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="short" direction="pas" swaplevel="3" rot="R180"/>
</symbol>
<symbol name="TESTPAD">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="RGB-LED">
<pin name="6" x="7.62" y="7.62" visible="off" length="middle" rot="R180"/>
<pin name="1" x="-7.62" y="7.62" visible="off" length="middle"/>
<pin name="5" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<pin name="2" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="4" x="7.62" y="-7.62" visible="off" length="middle" rot="R180"/>
<pin name="3" x="-7.62" y="-7.62" visible="off" length="middle"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.54" x2="-0.254" y2="2.286" width="0.254" layer="94"/>
<wire x1="-0.254" y1="2.286" x2="0" y2="3.048" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="4.826" x2="-1.27" y2="3.556" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.556" x2="-1.524" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.524" y1="3.302" x2="-1.27" y2="4.064" width="0.254" layer="94"/>
<wire x1="-1.27" y1="4.064" x2="-0.762" y2="3.556" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0.508" y1="-5.08" x2="-0.254" y2="-5.334" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-5.334" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0.508" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-2.794" x2="-1.27" y2="-4.064" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-4.064" x2="-1.524" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-4.318" x2="-1.27" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.556" x2="-0.762" y2="-4.064" width="0.254" layer="94"/>
<wire x1="1.27" y1="-11.43" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0.508" y1="-12.7" x2="-0.254" y2="-12.954" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-12.954" x2="0" y2="-12.192" width="0.254" layer="94"/>
<wire x1="0" y1="-12.192" x2="0.508" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-10.414" x2="-1.27" y2="-11.684" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-11.684" x2="-1.524" y2="-11.938" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-11.938" x2="-1.27" y2="-11.176" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-11.176" x2="-0.762" y2="-11.684" width="0.254" layer="94"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="-13.97" width="0.254" layer="94"/>
<wire x1="3.81" y1="-13.97" x2="-3.81" y2="-13.97" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-13.97" x2="-3.81" y2="11.43" width="0.254" layer="94"/>
<wire x1="-3.81" y1="11.43" x2="3.81" y2="11.43" width="0.254" layer="94"/>
<text x="7.62" y="8.89" size="0.8128" layer="94" rot="MR0">BLUE</text>
<text x="7.62" y="1.27" size="0.8128" layer="94" rot="MR0">RED</text>
<text x="7.62" y="-6.35" size="0.8128" layer="94" rot="MR0">GREEN</text>
<text x="-12.7" y="5.08" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="CPOL">
<wire x1="-1.524" y1="1.651" x2="1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.651" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<text x="1.143" y="3.0226" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="2.9464" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-2.0574" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0" x2="1.651" y2="0.889" layer="94"/>
<pin name="-" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="PINHEAD-2X3">
<pin name="1" x="-10.16" y="2.54" length="middle"/>
<pin name="2" x="-10.16" y="0" length="middle"/>
<pin name="3" x="-10.16" y="-2.54" length="middle"/>
<pin name="4" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="5" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="6" x="10.16" y="-2.54" length="middle" rot="R180"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/10V-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1863-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.031" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/16V" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-5514-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="C1210C476M4PACTU" constant="no"/>
<attribute name="MOUSER" value="80-C1210C476M4P" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2143-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.017" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.6PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-7944-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0193" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1254-1-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328P" prefix="U">
<gates>
<gate name="G$1" symbol="ATMEGA328" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP-32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="G$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="G$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC@4" pad="4"/>
<connect gate="G$1" pin="VCC@6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ATMEGA328P-AURCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="3.7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY_HOLDER" prefix="VBAT" uservalue="yes">
<description>&lt;b&gt;Coin Cell Holder&lt;/b&gt; :-  __BU2032SM-HD-G__  __BC-2400-ND__&lt;p&gt;
&lt;b&gt;AA  Battery Holder&lt;/b&gt; :- __2462K-ND__&lt;p&gt;
&lt;b&gt;LI-ION Holder &lt;/b&gt;:- __BH-18650-PC__</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA" package="2*AA_BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIM" package="AA-BATTERY-HOLDER-DIMENSION">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LI-ION" package="LI-BATTERY-HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BH-18650-PC " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.81" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2032" package="COINCELL-SMT">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4MM_PINS" package="COINCELL-DIP">
<connects>
<connect gate="G$1" pin="+" pad="+ +@1"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CLIP" package="BC-2400-ND">
<connects>
<connect gate="G$1" pin="+" pad="+@1 +@2"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-20MM_DIP" package="CR2032-THRU">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC2032-E2-ND &amp; N189-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.04" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3*AA" package="3-AA-BATTERY-HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC3AAPC-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.5" constant="no"/>
</technology>
</technologies>
</device>
<device name="CR2032-VERT" package="CR2032-VERT">
<connects>
<connect gate="G$1" pin="+" pad="+@1 +@2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BS-5-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.71" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.85" constant="no"/>
</technology>
</technologies>
</device>
<device name="-CLIP_SMT" package="BC-2003">
<connects>
<connect gate="G$1" pin="+" pad="+@0 +@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC-2003-ND " constant="no"/>
<attribute name="MOUSER" value="" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RETAINER_SMT" package="BK-912">
<connects>
<connect gate="G$1" pin="+" pad="+@0 +@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BK-912-ND " constant="no"/>
<attribute name="MOUSER" value="712-BAT-HLD-001 " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.29" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.34" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1XAA" package="1*AA_BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="36-2460-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.8649" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.57" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE" prefix="TAC">
<description>http://www.digikey.com/product-detail/en/FSM4JSMA/450-1129-ND/525821</description>
<gates>
<gate name="G$1" symbol="TACTILE" x="0" y="0"/>
</gates>
<devices>
<device name="-SMT" package="TACTILE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="450-1129-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIP" package="TACTILE-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="679-2428-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="TACTILE-SMD-4.5X4.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="EG5353CT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1823" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12X12" package="TAC-SMD-12X12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="EG4904DKR-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.4937" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6X6" package="TACTILE-SMD-6X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CKN9112CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LS_A67K">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="LS_Y876">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2512-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.09"/>
</technology>
</technologies>
</device>
<device name="-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-3118-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YELLOW" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-1196-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-BLUE" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2816-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IR" package="IR-LED">
<connects>
<connect gate="G$1" pin="A" pad="+"/>
<connect gate="G$1" pin="C" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1080-1076-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1628" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.39" constant="no"/>
</technology>
</technologies>
</device>
<device name="RED" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5005-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-GRN" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1125-1186-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.125" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.33" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YLW" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5010-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;Resistance&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0603-50-E3 "/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9.53K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-9.53KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-49.9K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-470GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.55K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.55KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.2K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.2KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-348K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-348KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-27GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR_NPN" prefix="Q">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2222" package="SOT23-R">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="568-1738-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.12" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SOT23-W" package="SOT23-W">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BC817-25-7-F" package="SOT23-W">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC817-25FDICT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-S8050" package="SOT23-W">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMSS8050-L-TPMSCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1187" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW_DIP-3" prefix="SW">
<gates>
<gate name="G$1" symbol="SW_DIP-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EDG-03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-3833-5-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.4" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TESTPAD" prefix="TP">
<gates>
<gate name="G$1" symbol="TESTPAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part. Plain copper pads"/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RGB-LED" prefix="RGB">
<gates>
<gate name="G$1" symbol="RGB-LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LRTB-GFTM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CLY6D-FKC-CK1N1D1BB7D3D3CT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.432" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.49" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL" prefix="CPOL" uservalue="yes">
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="-2.54"/>
</gates>
<devices>
<device name="_B" package="CPOL_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_D" package="CPOL_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CPOL-G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-F" package="CPOL_F_8X10">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E" package="CPOL_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-16X16" package="CPOL-16X16">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TANTALUM-100UF/6.3V" package="EIA3528-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-8156-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.61"/>
</technology>
</technologies>
</device>
<device name="-470UF/25V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-2584-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.56"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-0.041OHM" package="E5-12.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-6062-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.77"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_100UF/10V/0.08OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-8497-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.49"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_330UF/6.3V/0.04OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10400-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.69"/>
</technology>
</technologies>
</device>
<device name="-1000UF/10V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-10963-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.4"/>
</technology>
</technologies>
</device>
<device name="-220UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6109-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.32" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6592-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="-330UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-1083-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.37" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-19MM" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="565-1579-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_220UF/16V" package="C2917">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10429-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM-10UF/6.3V-1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-2181-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/6.3V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6602-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/25V" package="E3.5-8">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6567-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/10V" package="E2.5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6565-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-1879-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.25" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6600-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.0509" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM-10UF/10V" package="C1206-POL">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="718-1118-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.128" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.31" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/50V" package="CPOL_10.3X10.3X10">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-9940-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.3747" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.81" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/16V" package="E2.5-6.3-7">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P833-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0955" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.25" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/25V" package="E2.5-8/6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-14503-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.35" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470UF/10V" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10524-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="1.2613" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.04" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-2X3">
<gates>
<gate name="G$1" symbol="PINHEAD-2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEAD-2X3-SMT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.127" drill="0.127">
<clearance class="0" value="0.127"/>
</class>
<class number="4" name="aref" width="0.4064" drill="0">
</class>
</classes>
<parts>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SW1" library="BaseApp" deviceset="SW_DIP-3" device=""/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun" deviceset="GND" device=""/>
<part name="C3" library="BaseApp" deviceset="CAPACITOR" device="-10UF/6.3V" value="10UF"/>
<part name="C4" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V" value="1UF"/>
<part name="C6" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF"/>
<part name="GND6" library="SparkFun" deviceset="GND" device=""/>
<part name="C5" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value="0.1UF"/>
<part name="GND5" library="SparkFun" deviceset="GND" device=""/>
<part name="R10" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="ATMEGA328P" library="BaseApp" deviceset="ATMEGA328P" device=""/>
<part name="VBAT101" library="BaseApp" deviceset="BATTERY_HOLDER" device="-1XAA"/>
<part name="C1" library="BaseApp" deviceset="CAPACITOR" device="-47UF/6.3V" value="47UF"/>
<part name="U$1" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND1" library="BaseApp" deviceset="GND" device=""/>
<part name="U$2" library="BaseApp" deviceset="VDD" device=""/>
<part name="U$3" library="BaseApp" deviceset="VDD" device=""/>
<part name="U$4" library="BaseApp" deviceset="VDD" device=""/>
<part name="U$5" library="BaseApp" deviceset="VDD" device=""/>
<part name="VBAT100" library="BaseApp" deviceset="BATTERY_HOLDER" device="-1XAA"/>
<part name="LEFT" library="BaseApp" deviceset="TACTILE" device="-12X12" value="TACTILE-12X12"/>
<part name="RIGHT" library="BaseApp" deviceset="TACTILE" device="-12X12" value="TACTILE-12X12"/>
<part name="DOWN" library="BaseApp" deviceset="TACTILE" device="-12X12" value="TACTILE-12X12"/>
<part name="UP" library="BaseApp" deviceset="TACTILE" device="-12X12" value="TACTILE-12X12"/>
<part name="LED1" library="BaseApp" deviceset="LED" device="-5MM" value="LED-5MM"/>
<part name="LED4" library="BaseApp" deviceset="LED" device="-5MM" value="LED-5MM"/>
<part name="LED2" library="BaseApp" deviceset="LED" device="-5MM" value="LED-5MM"/>
<part name="R2" library="BaseApp" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="T1" library="BaseApp" deviceset="TRANSISTOR_NPN" device="-2222"/>
<part name="R9" library="BaseApp" deviceset="RESISTOR" device="-4.7K_1/8W" value="4.7K"/>
<part name="R7" library="BaseApp" deviceset="RESISTOR" device="-47K_1/8W" value="47K"/>
<part name="GND22" library="BaseApp" deviceset="GND" device=""/>
<part name="R15" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R8" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R19" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R12" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="U$6" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="F5" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="F2" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="F6" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="F3" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="R21" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R17" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R22" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R18" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="U$7" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="F1" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="F4" library="BaseApp" deviceset="TACTILE" device="" value="TACTILE"/>
<part name="R16" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R20" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="U$8" library="BaseApp" deviceset="VDD" device=""/>
<part name="C2" library="BaseApp" deviceset="CAPACITOR" device="-47UF/6.3V" value="47UF"/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="LED3" library="BaseApp" deviceset="LED" device="-5MM" value="LED-5MM"/>
<part name="R3" library="BaseApp" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="R5" library="BaseApp" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="R4" library="BaseApp" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="R11" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="VDD" library="BaseApp" deviceset="TESTPAD" device=""/>
<part name="RX" library="BaseApp" deviceset="TESTPAD" device=""/>
<part name="TX" library="BaseApp" deviceset="TESTPAD" device=""/>
<part name="GND" library="BaseApp" deviceset="TESTPAD" device=""/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="RGB" library="BaseApp" deviceset="RGB-LED" device=""/>
<part name="R13" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="R14" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="T2" library="BaseApp" deviceset="TRANSISTOR_NPN" device="-2222"/>
<part name="R1" library="BaseApp" deviceset="RESISTOR" device="-4.7K_1/8W" value="4.7K"/>
<part name="R6" library="BaseApp" deviceset="RESISTOR" device="-47K_1/8W" value="47K"/>
<part name="GND9" library="BaseApp" deviceset="GND" device=""/>
<part name="CPOL1" library="BaseApp" deviceset="CPOL" device="-470UF/10V" value="470UF"/>
<part name="U$9" library="BaseApp" deviceset="PINHEAD-2X3" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="2.54" y1="88.9" x2="53.34" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="88.9" x2="127" y2="96.52" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="96.52" x2="127" y2="132.08" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="132.08" x2="127" y2="177.8" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="177.8" x2="2.54" y2="177.8" width="1.27" layer="94" style="shortdash"/>
<wire x1="2.54" y1="177.8" x2="2.54" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="2.54" y1="88.9" x2="2.54" y2="2.54" width="1.27" layer="94" style="shortdash"/>
<wire x1="2.54" y1="2.54" x2="53.34" y2="2.54" width="1.27" layer="94" style="shortdash"/>
<wire x1="53.34" y1="2.54" x2="53.34" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="53.34" y1="88.9" x2="127" y2="88.9" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="88.9" x2="127" y2="25.4" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="25.4" x2="127" y2="2.54" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="2.54" x2="53.34" y2="2.54" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="96.52" x2="185.42" y2="96.52" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="96.52" x2="256.54" y2="96.52" width="1.27" layer="94" style="shortdash"/>
<wire x1="256.54" y1="96.52" x2="256.54" y2="121.92" width="1.27" layer="94" style="shortdash"/>
<wire x1="256.54" y1="121.92" x2="256.54" y2="177.8" width="1.27" layer="94" style="shortdash"/>
<wire x1="256.54" y1="177.8" x2="185.42" y2="177.8" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="177.8" x2="127" y2="177.8" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="177.8" x2="185.42" y2="132.08" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="132.08" x2="185.42" y2="121.92" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="121.92" x2="185.42" y2="96.52" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="132.08" x2="185.42" y2="132.08" width="1.27" layer="94" style="shortdash"/>
<wire x1="127" y1="25.4" x2="256.54" y2="25.4" width="1.27" layer="94" style="shortdash"/>
<wire x1="256.54" y1="25.4" x2="256.54" y2="96.52" width="1.27" layer="94" style="shortdash"/>
<wire x1="185.42" y1="121.92" x2="256.54" y2="121.92" width="1.27" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="GND2" gate="1" x="251.46" y="102.87" smashed="yes">
<attribute name="VALUE" x="248.92" y="100.33" size="1.778" layer="96"/>
</instance>
<instance part="SW1" gate="G$1" x="163.83" y="157.48" rot="MR0"/>
<instance part="GND11" gate="1" x="173.99" y="148.59" smashed="yes" rot="MR0">
<attribute name="VALUE" x="180.34" y="148.59" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND4" gate="1" x="45.72" y="101.6"/>
<instance part="C3" gate="G$1" x="25.4" y="157.48"/>
<instance part="C4" gate="G$1" x="17.78" y="157.48"/>
<instance part="C6" gate="G$1" x="7.62" y="157.48"/>
<instance part="GND6" gate="1" x="17.78" y="133.35"/>
<instance part="C5" gate="G$1" x="25.4" y="139.7"/>
<instance part="GND5" gate="1" x="25.4" y="133.35"/>
<instance part="R10" gate="G$1" x="45.72" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="48.006" y="160.8074" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="48.006" y="165.608" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND10" gate="1" x="38.1" y="104.14"/>
<instance part="ATMEGA328P" gate="G$1" x="86.36" y="132.08"/>
<instance part="VBAT101" gate="G$1" x="20.32" y="43.18" rot="R90"/>
<instance part="C1" gate="G$1" x="27.94" y="35.56"/>
<instance part="U$1" gate="G$1" x="20.32" y="50.8"/>
<instance part="GND1" gate="G$1" x="20.32" y="22.86"/>
<instance part="U$2" gate="G$1" x="33.02" y="170.18"/>
<instance part="U$3" gate="G$1" x="38.1" y="165.1"/>
<instance part="U$4" gate="G$1" x="198.12" y="116.84"/>
<instance part="U$5" gate="G$1" x="38.1" y="121.92"/>
<instance part="VBAT100" gate="G$1" x="20.32" y="33.02" rot="R90"/>
<instance part="LEFT" gate="G$1" x="91.44" y="60.96"/>
<instance part="RIGHT" gate="G$1" x="91.44" y="50.8"/>
<instance part="DOWN" gate="G$1" x="91.44" y="40.64"/>
<instance part="UP" gate="G$1" x="91.44" y="33.02"/>
<instance part="LED1" gate="G$1" x="243.84" y="160.02" rot="R270"/>
<instance part="LED4" gate="G$1" x="243.84" y="154.94" rot="R270"/>
<instance part="LED2" gate="G$1" x="243.84" y="165.1" rot="R270"/>
<instance part="R2" gate="G$1" x="228.6" y="160.02" rot="MR180"/>
<instance part="T1" gate="G$1" x="210.82" y="160.02"/>
<instance part="R9" gate="G$1" x="200.66" y="160.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="204.47" y="161.5186" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="204.47" y="156.718" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R7" gate="G$1" x="207.01" y="154.94" smashed="yes" rot="MR90">
<attribute name="NAME" x="208.5086" y="151.13" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="203.708" y="151.13" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND22" gate="G$1" x="209.55" y="147.32" rot="MR0"/>
<instance part="R15" gate="G$1" x="66.04" y="60.96" rot="R180"/>
<instance part="R8" gate="G$1" x="66.04" y="50.8" rot="R180"/>
<instance part="R19" gate="G$1" x="66.04" y="40.64" rot="R180"/>
<instance part="R12" gate="G$1" x="66.04" y="33.02" rot="R180"/>
<instance part="U$6" gate="G$1" x="58.42" y="66.04"/>
<instance part="GND3" gate="1" x="106.68" y="20.32" smashed="yes" rot="MR0">
<attribute name="VALUE" x="113.03" y="20.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="F5" gate="G$1" x="185.42" y="66.04"/>
<instance part="F2" gate="G$1" x="185.42" y="55.88"/>
<instance part="F6" gate="G$1" x="185.42" y="45.72"/>
<instance part="F3" gate="G$1" x="185.42" y="38.1"/>
<instance part="R21" gate="G$1" x="160.02" y="66.04" rot="R180"/>
<instance part="R17" gate="G$1" x="160.02" y="55.88" rot="R180"/>
<instance part="R22" gate="G$1" x="160.02" y="45.72" rot="R180"/>
<instance part="R18" gate="G$1" x="160.02" y="38.1" rot="R180"/>
<instance part="U$7" gate="G$1" x="152.4" y="91.44"/>
<instance part="GND7" gate="1" x="200.66" y="30.48" smashed="yes" rot="MR0">
<attribute name="VALUE" x="207.01" y="30.48" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="F1" gate="G$1" x="185.42" y="86.36"/>
<instance part="F4" gate="G$1" x="185.42" y="76.2"/>
<instance part="R16" gate="G$1" x="160.02" y="86.36" rot="R180"/>
<instance part="R20" gate="G$1" x="160.02" y="76.2" rot="R180"/>
<instance part="U$8" gate="G$1" x="251.46" y="167.64"/>
<instance part="C2" gate="G$1" x="35.56" y="35.56"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="LED3" gate="G$1" x="243.84" y="149.86" rot="R270"/>
<instance part="R3" gate="G$1" x="228.6" y="165.1" rot="MR180"/>
<instance part="R5" gate="G$1" x="228.6" y="154.94" rot="MR180"/>
<instance part="R4" gate="G$1" x="228.6" y="149.86" rot="MR180"/>
<instance part="R11" gate="G$1" x="149.86" y="119.38"/>
<instance part="VDD" gate="G$1" x="198.12" y="101.6" rot="R180"/>
<instance part="RX" gate="G$1" x="213.36" y="101.6" rot="R180"/>
<instance part="TX" gate="G$1" x="228.6" y="101.6" rot="R180"/>
<instance part="GND" gate="G$1" x="243.84" y="101.6" rot="R180"/>
<instance part="GND8" gate="1" x="180.34" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="186.69" y="101.6" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="RGB" gate="G$1" x="165.1" y="111.76" rot="R180"/>
<instance part="R13" gate="G$1" x="149.86" y="111.76"/>
<instance part="R14" gate="G$1" x="149.86" y="104.14"/>
<instance part="T2" gate="G$1" x="210.82" y="139.7"/>
<instance part="R1" gate="G$1" x="200.66" y="139.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="204.47" y="141.1986" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="204.47" y="136.398" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R6" gate="G$1" x="207.01" y="134.62" smashed="yes" rot="MR90">
<attribute name="NAME" x="208.5086" y="130.81" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="203.708" y="130.81" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND9" gate="G$1" x="209.55" y="127" rot="MR0"/>
<instance part="CPOL1" gate="G$1" x="43.18" y="35.56"/>
<instance part="U$9" gate="G$1" x="27.94" y="111.76"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="243.84" y1="104.14" x2="243.84" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="243.84" y1="106.68" x2="251.46" y2="106.68" width="0.1524" layer="91"/>
<wire x1="251.46" y1="106.68" x2="251.46" y2="105.41" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="4"/>
<wire x1="171.45" y1="160.02" x2="173.99" y2="160.02" width="0.1524" layer="91"/>
<wire x1="173.99" y1="160.02" x2="173.99" y2="157.48" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="5"/>
<wire x1="173.99" y1="157.48" x2="173.99" y2="154.94" width="0.1524" layer="91"/>
<wire x1="173.99" y1="154.94" x2="173.99" y2="151.13" width="0.1524" layer="91"/>
<wire x1="171.45" y1="157.48" x2="173.99" y2="157.48" width="0.1524" layer="91"/>
<junction x="173.99" y="157.48"/>
<pinref part="SW1" gate="G$1" pin="6"/>
<wire x1="171.45" y1="154.94" x2="173.99" y2="154.94" width="0.1524" layer="91"/>
<junction x="173.99" y="154.94"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="60.96" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="45.72" y1="111.76" x2="45.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="60.96" y1="109.22" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<wire x1="45.72" y1="109.22" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<junction x="45.72" y="111.76"/>
<wire x1="60.96" y1="106.68" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="45.72" y1="106.68" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="45.72" y="109.22"/>
<pinref part="ATMEGA328P" gate="G$1" pin="GND@3"/>
<pinref part="ATMEGA328P" gate="G$1" pin="GND@5"/>
<pinref part="ATMEGA328P" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="7.62" y1="154.94" x2="17.78" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="17.78" y1="154.94" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="17.78" y="154.94"/>
<wire x1="17.78" y1="154.94" x2="17.78" y2="135.89" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="25.4" y1="135.89" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="38.1" y1="109.22" x2="38.1" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="U$9" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="20.32" y1="25.4" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="25.4" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="27.94" y1="25.4" x2="27.94" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="27.94" y1="25.4" x2="35.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="35.56" y1="25.4" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<junction x="27.94" y="25.4"/>
<pinref part="VBAT100" gate="G$1" pin="-"/>
<wire x1="20.32" y1="25.4" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<junction x="20.32" y="25.4"/>
<pinref part="CPOL1" gate="G$1" pin="-"/>
<wire x1="35.56" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<wire x1="43.18" y1="25.4" x2="43.18" y2="33.02" width="0.1524" layer="91"/>
<junction x="35.56" y="25.4"/>
</segment>
<segment>
<pinref part="T1" gate="G$1" pin="E"/>
<wire x1="213.36" y1="154.94" x2="213.36" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="207.01" y1="149.86" x2="207.01" y2="147.32" width="0.1524" layer="91"/>
<wire x1="207.01" y1="147.32" x2="209.55" y2="147.32" width="0.1524" layer="91"/>
<pinref part="GND22" gate="G$1" pin="GND"/>
<wire x1="209.55" y1="147.32" x2="213.36" y2="147.32" width="0.1524" layer="91"/>
<junction x="209.55" y="147.32"/>
</segment>
<segment>
<pinref part="LEFT" gate="G$1" pin="4"/>
<wire x1="99.06" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="106.68" y1="58.42" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="LEFT" gate="G$1" pin="3"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<wire x1="106.68" y1="48.26" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="40.64" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="38.1" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="33.02" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<wire x1="106.68" y1="30.48" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<junction x="106.68" y="58.42"/>
<pinref part="RIGHT" gate="G$1" pin="3"/>
<wire x1="99.06" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<junction x="106.68" y="50.8"/>
<pinref part="RIGHT" gate="G$1" pin="4"/>
<wire x1="99.06" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<junction x="106.68" y="48.26"/>
<pinref part="DOWN" gate="G$1" pin="3"/>
<wire x1="99.06" y1="40.64" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="40.64"/>
<pinref part="DOWN" gate="G$1" pin="4"/>
<wire x1="99.06" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<junction x="106.68" y="38.1"/>
<pinref part="UP" gate="G$1" pin="3"/>
<wire x1="99.06" y1="33.02" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<junction x="106.68" y="33.02"/>
<pinref part="UP" gate="G$1" pin="4"/>
<wire x1="99.06" y1="30.48" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<junction x="106.68" y="30.48"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="F5" gate="G$1" pin="4"/>
<wire x1="193.04" y1="63.5" x2="200.66" y2="63.5" width="0.1524" layer="91"/>
<wire x1="200.66" y1="63.5" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<wire x1="200.66" y1="55.88" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="200.66" y1="53.34" x2="200.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="200.66" y1="45.72" x2="200.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="43.18" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<wire x1="200.66" y1="38.1" x2="200.66" y2="35.56" width="0.1524" layer="91"/>
<wire x1="200.66" y1="35.56" x2="200.66" y2="33.02" width="0.1524" layer="91"/>
<wire x1="200.66" y1="66.04" x2="200.66" y2="63.5" width="0.1524" layer="91"/>
<junction x="200.66" y="63.5"/>
<pinref part="F2" gate="G$1" pin="3"/>
<wire x1="193.04" y1="55.88" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<junction x="200.66" y="55.88"/>
<pinref part="F2" gate="G$1" pin="4"/>
<wire x1="193.04" y1="53.34" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<junction x="200.66" y="53.34"/>
<pinref part="F6" gate="G$1" pin="3"/>
<wire x1="193.04" y1="45.72" x2="200.66" y2="45.72" width="0.1524" layer="91"/>
<junction x="200.66" y="45.72"/>
<pinref part="F6" gate="G$1" pin="4"/>
<wire x1="193.04" y1="43.18" x2="200.66" y2="43.18" width="0.1524" layer="91"/>
<junction x="200.66" y="43.18"/>
<pinref part="F3" gate="G$1" pin="3"/>
<wire x1="193.04" y1="38.1" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<junction x="200.66" y="38.1"/>
<pinref part="F3" gate="G$1" pin="4"/>
<wire x1="193.04" y1="35.56" x2="200.66" y2="35.56" width="0.1524" layer="91"/>
<junction x="200.66" y="35.56"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="F5" gate="G$1" pin="3"/>
<wire x1="193.04" y1="66.04" x2="200.66" y2="66.04" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="4"/>
<wire x1="193.04" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="3"/>
<wire x1="200.66" y1="76.2" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="193.04" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<wire x1="200.66" y1="86.36" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<junction x="200.66" y="83.82"/>
<pinref part="F4" gate="G$1" pin="3"/>
<wire x1="193.04" y1="76.2" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<junction x="200.66" y="76.2"/>
<pinref part="F4" gate="G$1" pin="4"/>
<wire x1="193.04" y1="73.66" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="200.66" y1="66.04" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="200.66" y="66.04"/>
<junction x="200.66" y="73.66"/>
</segment>
<segment>
<wire x1="180.34" y1="111.76" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="104.14" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="180.34" y1="119.38" x2="180.34" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<junction x="180.34" y="104.14"/>
<pinref part="RGB" gate="G$1" pin="3"/>
<wire x1="172.72" y1="119.38" x2="180.34" y2="119.38" width="0.1524" layer="91"/>
<pinref part="RGB" gate="G$1" pin="2"/>
<wire x1="172.72" y1="111.76" x2="180.34" y2="111.76" width="0.1524" layer="91"/>
<junction x="180.34" y="111.76"/>
<pinref part="RGB" gate="G$1" pin="1"/>
<wire x1="172.72" y1="104.14" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="G$1" pin="E"/>
<wire x1="213.36" y1="134.62" x2="213.36" y2="127" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="207.01" y1="129.54" x2="207.01" y2="127" width="0.1524" layer="91"/>
<wire x1="207.01" y1="127" x2="209.55" y2="127" width="0.1524" layer="91"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
<wire x1="209.55" y1="127" x2="213.36" y2="127" width="0.1524" layer="91"/>
<junction x="209.55" y="127"/>
</segment>
</net>
<net name="IRC3" class="0">
<segment>
<wire x1="142.24" y1="154.94" x2="156.21" y2="154.94" width="0.1524" layer="91"/>
<label x="153.67" y="154.94" size="1.778" layer="95" rot="MR0"/>
<pinref part="SW1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD2(INT0)"/>
<wire x1="109.22" y1="134.62" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<label x="114.3" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="IRC2" class="0">
<segment>
<wire x1="142.24" y1="157.48" x2="156.21" y2="157.48" width="0.1524" layer="91"/>
<label x="153.67" y="157.48" size="1.778" layer="95" rot="MR0"/>
<pinref part="SW1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC5(ADC5/SCL)"/>
<wire x1="109.22" y1="149.86" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
<label x="114.3" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<wire x1="109.22" y1="106.68" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<label x="114.3" y="106.68" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PB4(MISO)"/>
</segment>
<segment>
<wire x1="5.08" y1="114.3" x2="17.78" y2="114.3" width="0.1524" layer="91"/>
<label x="5.08" y="114.3" size="1.778" layer="95"/>
<pinref part="U$9" gate="G$1" pin="1"/>
<junction x="17.78" y="114.3"/>
<wire x1="17.78" y1="114.3" x2="10.16" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<wire x1="25.4" y1="144.78" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
<label x="50.165" y="149.225" size="1.778" layer="95" rot="R180"/>
<wire x1="25.4" y1="147.32" x2="60.96" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="ATMEGA328P" gate="G$1" pin="AREF"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<wire x1="50.8" y1="162.56" x2="60.96" y2="162.56" width="0.1524" layer="91"/>
<label x="53.34" y="162.56" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PC6(/RESET)"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="5.08" y1="109.22" x2="17.78" y2="109.22" width="0.1524" layer="91"/>
<label x="5.08" y="109.22" size="1.778" layer="95"/>
<pinref part="U$9" gate="G$1" pin="3"/>
<junction x="17.78" y="109.22"/>
<wire x1="17.78" y1="109.22" x2="10.16" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="109.22" y1="109.22" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
<label x="114.3" y="109.22" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PB3(MOSI/OC2)"/>
</segment>
<segment>
<wire x1="38.1" y1="111.76" x2="43.18" y2="111.76" width="0.1524" layer="91"/>
<label x="38.1" y="111.76" size="1.778" layer="95"/>
<pinref part="U$9" gate="G$1" pin="5"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="109.22" y1="104.14" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PB5(SCK)"/>
</segment>
<segment>
<wire x1="5.08" y1="111.76" x2="17.78" y2="111.76" width="0.1524" layer="91"/>
<label x="5.08" y="111.76" size="1.778" layer="95"/>
<pinref part="U$9" gate="G$1" pin="2"/>
<junction x="17.78" y="111.76"/>
<wire x1="17.78" y1="111.76" x2="10.16" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IRC1" class="0">
<segment>
<wire x1="142.24" y1="160.02" x2="156.21" y2="160.02" width="0.1524" layer="91"/>
<label x="153.67" y="160.02" size="1.778" layer="95" rot="MR0"/>
<pinref part="SW1" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC4(ADC4/SDA)"/>
<wire x1="109.22" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="114.3" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<wire x1="109.22" y1="139.7" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<label x="114.3" y="139.7" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PD0(RXD)"/>
</segment>
<segment>
<wire x1="213.36" y1="104.14" x2="213.36" y2="116.84" width="0.1524" layer="91"/>
<label x="213.36" y="107.95" size="1.778" layer="95" rot="R90"/>
<pinref part="RX" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<wire x1="109.22" y1="137.16" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<label x="114.3" y="137.16" size="1.778" layer="95"/>
<pinref part="ATMEGA328P" gate="G$1" pin="PD1(TXD)"/>
</segment>
<segment>
<wire x1="228.6" y1="104.14" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<label x="228.6" y="107.95" size="1.778" layer="95" rot="R90"/>
<pinref part="TX" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="48.26" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="48.26" x2="27.94" y2="48.26" width="0.1524" layer="91"/>
<wire x1="27.94" y1="48.26" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="27.94" y1="48.26" x2="35.56" y2="48.26" width="0.1524" layer="91"/>
<wire x1="35.56" y1="48.26" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="27.94" y="48.26"/>
<pinref part="VBAT101" gate="G$1" pin="+"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<junction x="20.32" y="48.26"/>
<pinref part="CPOL1" gate="G$1" pin="+"/>
<wire x1="35.56" y1="48.26" x2="43.18" y2="48.26" width="0.1524" layer="91"/>
<wire x1="43.18" y1="48.26" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="48.26"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="7.62" y1="162.56" x2="17.78" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="17.78" y1="162.56" x2="25.4" y2="162.56" width="0.1524" layer="91"/>
<junction x="17.78" y="162.56"/>
<wire x1="60.96" y1="157.48" x2="33.02" y2="157.48" width="0.1524" layer="91"/>
<wire x1="33.02" y1="157.48" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="162.56" x2="33.02" y2="170.18" width="0.1524" layer="91"/>
<wire x1="60.96" y1="154.94" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="157.48" width="0.1524" layer="91"/>
<junction x="33.02" y="157.48"/>
<wire x1="60.96" y1="152.4" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<wire x1="33.02" y1="152.4" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="33.02" y="154.94"/>
<wire x1="25.4" y1="162.56" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<junction x="25.4" y="162.56"/>
<junction x="33.02" y="162.56"/>
<pinref part="ATMEGA328P" gate="G$1" pin="VCC@4"/>
<pinref part="ATMEGA328P" gate="G$1" pin="VCC@6"/>
<pinref part="ATMEGA328P" gate="G$1" pin="AVCC"/>
<pinref part="U$2" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="40.64" y1="162.56" x2="38.1" y2="162.56" width="0.1524" layer="91"/>
<wire x1="38.1" y1="162.56" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="VDD"/>
</segment>
<segment>
<wire x1="198.12" y1="104.14" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="VDD"/>
<pinref part="VDD" gate="G$1" pin="TP"/>
</segment>
<segment>
<wire x1="38.1" y1="114.3" x2="38.1" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="VDD"/>
<pinref part="U$9" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="60.96" y1="60.96" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="60.96" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="60.96"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="60.96" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="58.42" y="50.8"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="60.96" y1="33.02" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<junction x="58.42" y="40.64"/>
<pinref part="U$6" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="154.94" y1="66.04" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="152.4" y1="76.2" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="152.4" y1="86.36" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="55.88" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="55.88" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<junction x="152.4" y="66.04"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="154.94" y1="45.72" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="152.4" y1="45.72" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<junction x="152.4" y="55.88"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="154.94" y1="38.1" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="152.4" y1="38.1" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
<junction x="152.4" y="45.72"/>
<pinref part="U$7" gate="G$1" pin="VDD"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="154.94" y1="86.36" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<junction x="152.4" y="86.36"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="154.94" y1="76.2" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<junction x="152.4" y="76.2"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="VDD"/>
<junction x="251.46" y="167.64"/>
<wire x1="248.92" y1="160.02" x2="251.46" y2="160.02" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="248.92" y1="165.1" x2="251.46" y2="165.1" width="0.1524" layer="91"/>
<wire x1="251.46" y1="165.1" x2="251.46" y2="160.02" width="0.1524" layer="91"/>
<junction x="251.46" y="160.02"/>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="248.92" y1="154.94" x2="251.46" y2="154.94" width="0.1524" layer="91"/>
<wire x1="251.46" y1="154.94" x2="251.46" y2="160.02" width="0.1524" layer="91"/>
<wire x1="251.46" y1="160.02" x2="251.46" y2="167.64" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="248.92" y1="149.86" x2="251.46" y2="149.86" width="0.1524" layer="91"/>
<wire x1="251.46" y1="154.94" x2="251.46" y2="149.86" width="0.1524" layer="91"/>
<junction x="251.46" y="154.94"/>
</segment>
</net>
<net name="IR-LED" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="195.58" y1="160.02" x2="187.96" y2="160.02" width="0.1524" layer="91"/>
<label x="187.96" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD3(INT1)"/>
<wire x1="109.22" y1="132.08" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<label x="114.3" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="195.58" y1="139.7" x2="187.96" y2="139.7" width="0.1524" layer="91"/>
<label x="187.96" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="T1" gate="G$1" pin="B"/>
<wire x1="208.28" y1="160.02" x2="207.01" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="207.01" y1="160.02" x2="205.74" y2="160.02" width="0.1524" layer="91"/>
<junction x="207.01" y="160.02"/>
</segment>
</net>
<net name="T1" class="0">
<segment>
<pinref part="LEFT" gate="G$1" pin="1"/>
<pinref part="LEFT" gate="G$1" pin="2"/>
<wire x1="83.82" y1="60.96" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<wire x1="83.82" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="83.82" y="60.96"/>
<pinref part="R15" gate="G$1" pin="1"/>
<label x="73.66" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC0(ADC0)"/>
<wire x1="109.22" y1="162.56" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<label x="114.3" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="T2" class="0">
<segment>
<pinref part="RIGHT" gate="G$1" pin="1"/>
<wire x1="83.82" y1="50.8" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="RIGHT" gate="G$1" pin="2"/>
<wire x1="83.82" y1="50.8" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<junction x="83.82" y="50.8"/>
<pinref part="R8" gate="G$1" pin="1"/>
<label x="73.66" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC1(ADC1)"/>
<wire x1="109.22" y1="160.02" x2="121.92" y2="160.02" width="0.1524" layer="91"/>
<label x="114.3" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="T3" class="0">
<segment>
<pinref part="DOWN" gate="G$1" pin="1"/>
<wire x1="83.82" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<pinref part="DOWN" gate="G$1" pin="2"/>
<wire x1="83.82" y1="40.64" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<junction x="83.82" y="40.64"/>
<pinref part="R19" gate="G$1" pin="1"/>
<label x="73.66" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC2(ADC2)"/>
<wire x1="109.22" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<label x="114.3" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="T4" class="0">
<segment>
<pinref part="UP" gate="G$1" pin="1"/>
<wire x1="83.82" y1="33.02" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<pinref part="UP" gate="G$1" pin="2"/>
<wire x1="83.82" y1="33.02" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
<junction x="83.82" y="33.02"/>
<pinref part="R12" gate="G$1" pin="1"/>
<label x="73.66" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PC3(ADC3)"/>
<wire x1="109.22" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<label x="114.3" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="F3" class="0">
<segment>
<pinref part="F5" gate="G$1" pin="1"/>
<pinref part="F5" gate="G$1" pin="2"/>
<wire x1="177.8" y1="66.04" x2="177.8" y2="63.5" width="0.1524" layer="91"/>
<wire x1="177.8" y1="66.04" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
<junction x="177.8" y="66.04"/>
<pinref part="R21" gate="G$1" pin="1"/>
<label x="170.18" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD7(AIN1)"/>
<wire x1="109.22" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<label x="114.3" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="F4" class="0">
<segment>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="177.8" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$1" pin="2"/>
<wire x1="177.8" y1="55.88" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="177.8" y="55.88"/>
<pinref part="R17" gate="G$1" pin="1"/>
<label x="170.18" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PB0(ICP)"/>
<wire x1="109.22" y1="116.84" x2="121.92" y2="116.84" width="0.1524" layer="91"/>
<label x="114.3" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="F5" class="0">
<segment>
<pinref part="F6" gate="G$1" pin="1"/>
<wire x1="177.8" y1="45.72" x2="165.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="F6" gate="G$1" pin="2"/>
<wire x1="177.8" y1="45.72" x2="177.8" y2="43.18" width="0.1524" layer="91"/>
<junction x="177.8" y="45.72"/>
<pinref part="R22" gate="G$1" pin="1"/>
<label x="170.18" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PB1(OC1A)"/>
<wire x1="109.22" y1="114.3" x2="121.92" y2="114.3" width="0.1524" layer="91"/>
<label x="114.3" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="F6" class="0">
<segment>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="177.8" y1="38.1" x2="165.1" y2="38.1" width="0.1524" layer="91"/>
<pinref part="F3" gate="G$1" pin="2"/>
<wire x1="177.8" y1="38.1" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="177.8" y="38.1"/>
<pinref part="R18" gate="G$1" pin="1"/>
<label x="170.18" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PB2(SS/OC1B)"/>
<wire x1="109.22" y1="111.76" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<label x="114.3" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="F1" class="0">
<segment>
<pinref part="F1" gate="G$1" pin="1"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<wire x1="177.8" y1="86.36" x2="165.1" y2="86.36" width="0.1524" layer="91"/>
<junction x="177.8" y="86.36"/>
<pinref part="R16" gate="G$1" pin="1"/>
<label x="170.18" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD4(XCK/T0)"/>
<wire x1="109.22" y1="129.54" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
<label x="114.3" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="F2" class="0">
<segment>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="177.8" y1="76.2" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="F4" gate="G$1" pin="2"/>
<wire x1="177.8" y1="76.2" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<junction x="177.8" y="76.2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<label x="170.18" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD5(T1)"/>
<wire x1="109.22" y1="127" x2="121.92" y2="127" width="0.1524" layer="91"/>
<label x="114.3" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="VBAT100" gate="G$1" pin="+"/>
<pinref part="VBAT101" gate="G$1" pin="-"/>
<wire x1="20.32" y1="35.56" x2="20.32" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<wire x1="241.3" y1="149.86" x2="233.68" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="241.3" y1="165.1" x2="233.68" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="233.68" y1="160.02" x2="241.3" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="LED4" gate="G$1" pin="C"/>
<wire x1="233.68" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="223.52" y1="165.1" x2="223.52" y2="160.02" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="C"/>
<junction x="223.52" y="165.1"/>
<wire x1="213.36" y1="165.1" x2="223.52" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GREEN" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="144.78" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<label x="134.62" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PD6(AIN0)"/>
<wire x1="109.22" y1="124.46" x2="121.92" y2="124.46" width="0.1524" layer="91"/>
<label x="114.3" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="RGB" gate="G$1" pin="4"/>
<wire x1="154.94" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="RGB" gate="G$1" pin="5"/>
<wire x1="154.94" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="RGB" gate="G$1" pin="6"/>
<wire x1="154.94" y1="104.14" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RED" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="144.78" y1="111.76" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<label x="137.16" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PB6(XTAL1/TOSC1)"/>
<wire x1="60.96" y1="137.16" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
<label x="53.34" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="BLUE" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="144.78" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<label x="137.16" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ATMEGA328P" gate="G$1" pin="PB7(XTAL2/TOSC2)"/>
<wire x1="60.96" y1="132.08" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<label x="53.34" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="223.52" y1="149.86" x2="223.52" y2="154.94" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="C"/>
<wire x1="213.36" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="144.78" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="215.9" y1="154.94" x2="223.52" y2="154.94" width="0.1524" layer="91"/>
<junction x="223.52" y="154.94"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="T2" gate="G$1" pin="B"/>
<wire x1="208.28" y1="139.7" x2="207.01" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="207.01" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<junction x="207.01" y="139.7"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,38.1,114.3,ISP,VCC,VDD,,,"/>
<approved hash="113,1,17.8731,45.0574,VBAT101,,,,,"/>
<approved hash="113,1,17.8731,34.8974,VBAT100,,,,,"/>
<approved hash="113,1,130.071,89.431,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
